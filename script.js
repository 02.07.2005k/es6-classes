"use strict"

//1) Прототип: Кожен об'єкт має прихований зв'язок з іншим об'єктом, який називається його прототипом.

// Ланцюг прототипів: Якщо властивість не знайдена в об'єкті,
// JavaScript шукає її у прототипі, і так далі по ланцюгу прототипів, поки не знайде 
// або не дійде до кінця ланцюга (де прототип дорівнює null).

//Object.prototype: Усі об'єкти успадковують властивості та методи від Object.prototype, якщо не вказано інше.

// 2) Виклик super() у конструкторі класу-нащадка потрібен для ініціалізації базового класу: Виконання конструктора батьківського класу для ініціалізації властивостей.
// Доступу до this: Виклик super() повинен бути перед використанням this в конструкторі нащадка.
// Спадкування властивостей: Передача аргументів до базового конструктора для належного наслідування властивостей.

let programmerIndex = 1;

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }


    get name() {
        return this._name;
    }

 
    set name(name) {
        this._name = name;
    }

 
    get age() {
        return this._age;
    }

   
    set age(age) {
        this._age = age;
    }


    get salary() {
        return this._salary;
    }


    set salary(salary) {
        this._salary = salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
        this._index = programmerIndex++;
    }

    get lang() {
        return this._lang;
    }


    set lang(lang) {
        this._lang = lang;
    }

 
    get salary() {
        return this._salary * 3;
    }

 
    get index() {
        return this._index;
    }
}


const programmer1 = new Programmer('Sergey', 30, 5000, ['JavaScript', 'Python']);
const programmer2 = new Programmer('Katya', 25, 4000, ['Java', 'C++']);
const programmer3 = new Programmer('Yarik', 18, 3000, ['HTML', 'CSS']);

console.log(`Programmer 1: Index: ${programmer1.index}, Name: ${programmer1.name}, Age: ${programmer1.age}, Salary: ${programmer1.salary}, Languages: ${programmer1.lang}`);
console.log(`Programmer 2: Index: ${programmer2.index}, Name: ${programmer2.name}, Age: ${programmer2.age}, Salary: ${programmer2.salary}, Languages: ${programmer2.lang}`);
console.log(`Programmer 3: Index: ${programmer3.index}, Name: ${programmer3.name}, Age: ${programmer3.age}, Salary: ${programmer3.salary}, Languages: ${programmer3.lang}`);
 